package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChiliSauce extends Food {
    Food food;

    public ChiliSauce(Food food) {
        this.food = food;
        this.description = ", adding chili sauce";
    }

    @Override
    public String getDescription() {
        return this.food.getDescription() + this.description;
    }

    @Override
    public double cost() {
        return this.food.cost() + 0.30;
    }
}
