package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

import java.util.ArrayList;
import java.util.List;

public class MainDecorator {
    public static void main(String[] args) {
        List<Food> breads = new ArrayList<Food>();
        breads.add(BreadProducer.THICK_BUN.createBreadToBeFilled());
        breads.add(BreadProducer.THIN_BUN.createBreadToBeFilled());
        breads.add(BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled());
        breads.add(BreadProducer.NO_CRUST_SANDWICH.createBreadToBeFilled());

        for (int i = 0; i < breads.size(); i++) {
            breads.set(i, FillingDecorator.BEEF_MEAT.addFillingToBread(breads.get(i)));
            breads.set(i, FillingDecorator.CHEESE.addFillingToBread(breads.get(i)));
            breads.set(i, FillingDecorator.CHICKEN_MEAT.addFillingToBread(breads.get(i)));
            breads.set(i, FillingDecorator.CHILI_SAUCE.addFillingToBread(breads.get(i)));
            breads.set(i, FillingDecorator.CUCUMBER.addFillingToBread(breads.get(i)));
            breads.set(i, FillingDecorator.LETTUCE.addFillingToBread(breads.get(i)));
            breads.set(i, FillingDecorator.TOMATO.addFillingToBread(breads.get(i)));
            breads.set(i, FillingDecorator.TOMATO_SAUCE.addFillingToBread(breads.get(i)));
            System.out.println(breads.get(i).getDescription());
            System.out.println("Total cost is " + breads.get(i).cost());
        }
    }
}
