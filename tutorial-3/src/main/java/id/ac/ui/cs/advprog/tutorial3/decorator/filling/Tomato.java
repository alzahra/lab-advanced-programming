package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Tomato extends Food {
    Food food;

    public Tomato(Food food) {
        this.food = food;
        this.description = ", adding tomato";
    }

    @Override
    public String getDescription() {
        return this.food.getDescription() + this.description;
    }

    @Override
    public double cost() {
        return this.food.cost() + 0.50;
    }
}
