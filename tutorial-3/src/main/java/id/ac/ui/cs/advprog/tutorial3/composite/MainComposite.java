package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class MainComposite {
    public static void main(String[] args) {
        Company company = new Company();

        Ceo luffy = new Ceo("Luffy", 500000.00);
        company.addEmployee(luffy);

        Cto zorro = new Cto("Zorro", 320000.00);
        company.addEmployee(zorro);

        BackendProgrammer franky = new BackendProgrammer("Franky", 94000.00);
        company.addEmployee(franky);

        FrontendProgrammer nami = new FrontendProgrammer("Nami", 66000.00);
        company.addEmployee(nami);

        UiUxDesigner sanji = new UiUxDesigner("sanji", 177000.00);
        company.addEmployee(sanji);

        NetworkExpert brook = new NetworkExpert("Brook", 83000.00);
        company.addEmployee(brook);

        SecurityExpert chopper = new SecurityExpert("Chopper", 90000.00);
        company.addEmployee(chopper);

        for (Employees e : company.getAllEmployees()) {
            System.out.println("Employee " + e.getName() + " has the role of " + e.getRole() + " and salary of " + e.getSalary());
        }

        System.out.println("\nTotal net salaries of the company is " + company.getNetSalaries());
    }
}
