package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.CamembertCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.BloodClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.NeapolitanCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.BarbecueSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

    public Dough createDough() {
        return new NeapolitanCrustDough();
    }

    public Sauce createSauce() {
        return new BarbecueSauce();
    }

    public Cheese createCheese() {
        return new CamembertCheese();
    }

    public Veggies[] createVeggies() {
        Veggies[] veggies = {new Broccoli(), new Spinach(), new BlackOlives(), new Eggplant()};
        return veggies;
    }

    public Clams createClam() {
        return new BloodClams();
    }
}
