package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class NeapolitanCrustDough implements Dough {
    public String toString() {
        return "Neapolitan crust dough";
    }
}
