package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BroccoliTest {
    private Broccoli broccoli;

    @Before
    public void setUp() {
        broccoli = new Broccoli();
    }

    @Test
    public void testMethodGetString() {
        assertEquals("Broccoli", broccoli.toString());
    }
}