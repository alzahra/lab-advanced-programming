package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ReggianoCheeseTest {
    private ReggianoCheese reggianoCheese;

    @Before
    public void setUp() {
        reggianoCheese = new ReggianoCheese();
    }

    @Test
    public void testMethodGetString() {
        assertEquals("Reggiano Cheese", reggianoCheese.toString());
    }
}