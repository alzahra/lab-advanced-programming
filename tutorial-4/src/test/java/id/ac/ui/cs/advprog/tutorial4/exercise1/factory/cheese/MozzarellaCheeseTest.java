package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MozzarellaCheeseTest {
    private MozzarellaCheese mozzarellaCheese;

    @Before
    public void setUp() {
        mozzarellaCheese = new MozzarellaCheese();
    }

    @Test
    public void testMethodGetString() {
        assertEquals("Shredded Mozzarella", mozzarellaCheese.toString());
    }
}