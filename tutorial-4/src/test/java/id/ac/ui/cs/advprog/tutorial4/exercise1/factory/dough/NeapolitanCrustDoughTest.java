package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NeapolitanCrustDoughTest {
    private NeapolitanCrustDough neapolitanCrustDough;

    @Before
    public void setUp() {
        neapolitanCrustDough = new NeapolitanCrustDough();
    }

    @Test
    public void testMethodGetString() {
        assertEquals("Neapolitan crust dough", neapolitanCrustDough.toString());
    }
}