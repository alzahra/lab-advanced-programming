package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DepokPizzaStoreTest {
    private DepokPizzaStore depokPizzaStore;

    @Before
    public void setUp() {
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void testMethodGetString() {
        Pizza pizza = depokPizzaStore.createPizza("cheese");
        assertEquals("Depok Style Cheese Pizza", pizza.getName());

        Pizza pizza2 = depokPizzaStore.createPizza("veggie");
        assertEquals("Depok Style Veggie Pizza", pizza2.getName());

        Pizza pizza3 = depokPizzaStore.createPizza("clam");
        assertEquals("Depok Style Clam Pizza", pizza3.getName());
    }
}