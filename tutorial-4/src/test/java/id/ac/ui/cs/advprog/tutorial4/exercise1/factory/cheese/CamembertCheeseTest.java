package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CamembertCheeseTest {
    private CamembertCheese camembertCheese;

    @Before
    public void setUp() {
        camembertCheese = new CamembertCheese();
    }

    @Test
    public void testMethodGetString() {
        assertEquals("Shredded Camembert", camembertCheese.toString());
    }
}