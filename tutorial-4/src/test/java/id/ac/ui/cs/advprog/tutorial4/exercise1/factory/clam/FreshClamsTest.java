package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FreshClamsTest {
    private FreshClams freshClams;

    @Before
    public void setUp() {
        freshClams = new FreshClams();
    }

    @Test
    public void testMethodGetString() {
        assertEquals("Fresh Clams from Long Island Sound", freshClams.toString());
    }
}