package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BarbecueSauceTest {
    private BarbecueSauce barbecueSauce;

    @Before
    public void setUp() {
        barbecueSauce = new BarbecueSauce();
    }

    @Test
    public void testMethodGetString() {
        assertEquals("Barbecue Sauce", barbecueSauce.toString());
    }
}