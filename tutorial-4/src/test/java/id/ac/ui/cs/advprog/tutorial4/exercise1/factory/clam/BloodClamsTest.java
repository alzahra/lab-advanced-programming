package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BloodClamsTest {
    private BloodClams bloodClams;

    @Before
    public void setUp() {
        bloodClams = new BloodClams();
    }

    @Test
    public void testMethodGetString() {
        assertEquals("Blood Clams from Brebes", bloodClams.toString());
    }
}