package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NewYorkPizzaStoreTest {
    private NewYorkPizzaStore newYorkPizzaStore;

    @Before
    public void setUp() {
        newYorkPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void testMethodGetString() {
        Pizza pizza = newYorkPizzaStore.createPizza("cheese");
        assertEquals("New York Style Cheese Pizza", pizza.getName());

        Pizza pizza2 = newYorkPizzaStore.createPizza("veggie");
        assertEquals("New York Style Veggie Pizza", pizza2.getName());

        Pizza pizza3 = newYorkPizzaStore.createPizza("clam");
        assertEquals("New York Style Clam Pizza", pizza3.getName());
    }
}