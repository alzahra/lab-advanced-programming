package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParmesanCheeseTest {
    private ParmesanCheese parmesanCheese;

    @Before
    public void setUp() {
        parmesanCheese = new ParmesanCheese();
    }

    @Test
    public void testMethodGetString() {
        assertEquals("Shredded Parmesan", parmesanCheese.toString());
    }
}